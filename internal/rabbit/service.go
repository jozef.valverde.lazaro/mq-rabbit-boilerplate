package rabbit

import (
	"fmt"

	"github.com/streadway/amqp"
)

// Service is the implementation of domain.MessageBrokerService
type Service struct {
	config *Config
}

//NewService return new service
func NewService(config *Config) Service {
	return Service{
		config: config,
	}
}

// GetConnection get amqp conn
func (s Service) GetConnection() (*amqp.Connection, error) {
	return amqp.Dial(s.config.URL)

}

//CreateChannel create new channel
func (s Service) CreateChannel(conn *amqp.Connection) (*amqp.Channel, error) {
	return conn.Channel()
}

// ExchangeDeclare todo
func (s Service) ExchangeDeclare(channel *amqp.Channel) error {
	return channel.ExchangeDeclare("events", "topic", true, false, false, false, nil)
}

// QueueDeclare declare function
func (s Service) QueueDeclare(channel *amqp.Channel) error {
	_, err := channel.QueueDeclare("test", true, false, false, false, nil)
	if err != nil {
		return err
	}
	err = channel.QueueBind("test", "random-key", "events", false, nil)

	if err != nil {
		return err
	}
	return nil
}

//Consume consume msg
func (s Service) Consume(channel *amqp.Channel) {
	// We consume data from the queue named Test using the channel we created in go.
	msgs, err := channel.Consume("test", "", false, false, false, false, nil)

	if err != nil {
		panic("error consuming the queue: " + err.Error())
	}

	// We loop through the messages in the queue and print them in the console.
	// The msgs will be a go channel, not an amqp channel
	for msg := range msgs {
		fmt.Println("message received: " + string(msg.Body))
		msg.Ack(false)
	}
}

// Publish publish msg
func (s Service) Publish(channel *amqp.Channel) error {
	message := amqp.Publishing{
		Body: []byte("Hello World"),
	}

	// We publish the message to the exahange we created earlier
	return channel.Publish("events", "random-key", false, false, message)
}

// CloseConnection close the connection
func (s Service) CloseConnection(conn *amqp.Connection) {
	conn.Close()
}
