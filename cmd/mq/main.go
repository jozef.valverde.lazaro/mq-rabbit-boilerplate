package main

import (
	"fmt"
	"os"

	"gitlab.com/jozef.valverde.lazaro/mq-rabbit-boilerplate/internal/rabbit"
)

func main() {
	url := os.Getenv("AMQP_URL")

	if url == "" {
		url = "amqp://guest:guest@localhost:5672"
	}
	config := rabbit.Config{
		URL: url,
	}
	rabbitService := rabbit.NewService(&config)
	conn, err := rabbitService.GetConnection()
	if err != nil {
		panic(err)
	}
	channel, err := rabbitService.CreateChannel(conn)
	if err != nil {
		panic(err)
	}
	if err := rabbitService.ExchangeDeclare(channel); err != nil {
		panic(err)
	}
	if err := rabbitService.QueueDeclare(channel); err != nil {
		panic(err)
	}

	rabbitService.Consume(channel)
	/*if err := rabbitService.Publish(channel); err != nil {
		panic(err)
	} */
	fmt.Println("published")
}
